% Wie macht GitLab mit GitLab GitLab?
% Winnie Hellmann
% 2019-04-23

# Wie macht GitLab mit GitLab GitLab? {.white-text data-background-image="https://about.gitlab.com/images/demo/demo-background-red.png" data-transition="zoom"}

<script src="title-slide-hack.js"></script>

Winnie Hellmann

2019-04-23

# DevOps cycle\ {data-transition="slide-in fade-out"}

![devops cycle diagram](devops-loop-and-spans-small.png){.bonus-padding}\

<https://about.gitlab.com/stages-devops-lifecycle/>

# DevOps cycle\ {data-transition="none"}

![devops cycle diagram with dev stages highlighted](dev-stages.png){.bonus-padding}\

<https://about.gitlab.com/stages-devops-lifecycle/>

# Plan\

![devops cycle diagram with Plan highlighted](plan.png){.bonus-padding}\

<https://about.gitlab.com/stages-devops-lifecycle/plan/>

## New Issue {.no-heading}

![screenshot of a bug report](bug-report.png)

<https://about.gitlab.com/handbook/communication/#everything-starts-with-an-issue>

<https://docs.gitlab.com/ee/user/project/description_templates.html>

## Labels

![screenshot of labels on an issue](issue-labels.png)

<https://docs.gitlab.com/ee/development/contributing/issue_workflow.html>

<https://docs.gitlab.com/ee/user/project/labels.html>

## Milestones

:::::::::::::: {.columns}
::: {.column width="50%"}
![screenshot of no assigned milestone](milestone-none.png) 
![screenshot of awaiting demand milestone](milestone-awaiting-demand.png)
:::
::: {.column width="50%"}
![screenshot of backlog milestone](milestone-backlog.png)
![screenshot of release milestone](milestone-release.png)
:::
::::::::::::::

<https://about.gitlab.com/handbook/engineering/workflow/#scheduling-issues>

<https://docs.gitlab.com/ee/user/project/milestones/>

## Issue Board

[![screenshot of issues in an issue board](issue-board.png)](https://gitlab.com/groups/gitlab-org/-/boards/1030832?label_name[]=frontend)

<https://docs.gitlab.com/ee/user/project/issue_board.html>

## Issue Assignee(s)

:::::::::::::: {.columns}
::: {.column width="50%"}
![screenshot of issue assignee](issue-assignee.png)
:::
::: {.column width="50%"}
![screenshot of multiple issue assignees](issue-multiple-assignees.png)
:::
::::::::::::::

<https://docs.gitlab.com/ee/user/project/issues/issue_data_and_actions.html#3-assignee>

<https://docs.gitlab.com/ee/user/project/issues/multiple_assignees_for_issues.html>

## Issue Dashboard

![screenshot of the issue dashboard](issue-dashboard.png)

<https://docs.gitlab.com/ee/user/profile/preferences.html#default-dashboard>
<https://gitlab.com/gitlab-org/gitlab-ce/issues/60755>

## Workflow Labels

![screenshot of issue board showing workflow labels](issue-board-workflow-labels.png)

<https://about.gitlab.com/handbook/engineering/workflow/>

<https://docs.gitlab.com/ee/user/project/labels.html#scoped-labels-premium>

# Create\
        
![devops cycle diagram with Create highlighted](create.png){.bonus-padding}\

<https://about.gitlab.com/stages-devops-lifecycle/create/>

## coden, coden, coden...

![animation of a cat typing on a laptop](cat-typing.gif)

## Git it

```shell
$ git push
Enumerating objects: 87, done.
Counting objects: 100% (87/87), done.
Delta compression using up to 12 threads
Compressing objects: 100% (53/53), done.
Writing objects: 100% (53/53), 5.08 KiB | 69.00 KiB/s, done.
Total 53 (delta 48), reused 0 (delta 0)
remote:
remote: To create a merge request for winh-simplify-frontend-fixtures, visit:
remote:   https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/new?merge_request%5Bsource_branch%5D=winh-simplify-frontend-fixtures
remote:
```

## Merge Request

[![screenshot of new merge request](new-merge-request.png)](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/27531)


## Review Comments

![screenshot of review comment](review-comment.png)

<https://docs.gitlab.com/ee/development/code_review.html>

<https://docs.gitlab.com/ee/user/discussions/#merge-request-reviews-premium>

# Verify\

![devops cycle diagram with Verify highlighted](verify.png){.bonus-padding}\

<https://about.gitlab.com/stages-devops-lifecycle/verify/>

## Was ist Verify?

- Teil von GitLabs CI/CD (Continuous Integration / Continuous Delivery)
- ursprünglich mit GitLab CI separates Produkt
- inzwischen vollständig Integriert

## Wie sieht CI aus? {data-transition="slide-in fade-out"}

![many small characters running around busy with different tasks](infinite_battle.gif)

## Wie sieht CI aus? {data-transition="fade-in slide-out"}

![diagram of an assembly belt that represents CI pipelines](ci-cd-test-deploy-illustration_2x.png)

## Was macht CI?

- automatisierte Tests
- statische Code-Analyse
  - potentielle Bugs
  - Sicherheitslücken
  - Lizenzprobleme
- Code-Style prüfen

## Wie geht CI?

- in GitLab per Konfiguration in `.gitlab-ci.yml`
- üblicherweise eine pro Projekt
- prinzipiell eine pro Branch bzw. Commit möglich

## Wie geht CI?

- im einfachsten Fall:

```yaml
mein job:
  script:
    - echo "Nichts tun."
```

![screenshot of pipeline with one job](small-pipeline.png)

## Wie geht CI?

![screenshot of job log](small-pipeline-log.png)

## Wie geht Parallelität?

```yaml
mein job:
  script:
    - echo "Nichts tun."

dein job:
  script:
    - echo "Viel tun."
```

![screenshot of parallel pipeline jobs](parallel-jobs.png)

## Wie geht Parallelität?

```yaml
dein job:
  parallel: 5
  script:
    - echo "Aufgabe ${CI_NODE_INDEX} von ${CI_NODE_TOTAL}"
```

![screenshot of parallel pipeline jobs](many-parallel-jobs.png)

## Wie geht Unparallelität?

```yaml
mein job:
  stage: test
  script:
    - echo "Nichts tun."

dein job:
  stage: deploy
  script:
    - echo "Viel tun."
```

![screenshot of pipeline with two stages](two-stages.png)

## Wie geht Unparallelität?

```yaml
stages:
  - zuerst
  - zuzweit

mein job:
  stage: zuerst
  script:
    - echo "Nichts tun."

dein job:
  stage: zuzweit
  script:
    - echo "Viel tun."
```

## Wie entstehen neue Pipelines?

- der Storch bringt sie nicht
- jeder `git push` erzeugt eine neue Pipeline
- Pipelines können [manuell erzeugt werden](https://docs.gitlab.com/ee/ci/pipelines.html#manually-executing-pipelines)
- oder [per API](https://docs.gitlab.com/ee/api/pipelines.html#create-a-new-pipeline)
- cron-artige [Pipeline schedules](https://docs.gitlab.com/ee/user/project/pipelines/schedules.html)

## Trödeln

```yaml
moment:
  when: delayed
  start_in: 30 minutes
  script:
    - echo "Wer hat an der Uhr gedreht?"
```

<https://docs.gitlab.com/ee/ci/yaml/#whendelayed>

## Handarbeit

```yaml
galvanize:
  when: manual
  script:
    - echo "Push the button!"
```

![screenshot of manual pipeline job](manual-job.png)

<https://docs.gitlab.com/ee/ci/yaml/#whenmanual>

## Inklusion und Exklusion

```yaml
harte Arbeit:
  script:
    - echo "Schaffe, schaffe, Häusle baue"
  except:
    - master
  only:
    - apprentice
```

<https://docs.gitlab.com/ee/ci/yaml/#onlyexcept-basic>

## Advanced Awesomeness

- [Variablen](https://docs.gitlab.com/ee/ci/yaml/#variables): Verhalten und Geheimnisse
- [Artefakte](https://docs.gitlab.com/ee/ci/yaml/#artifacts): Dateien ausspucken
- [Caching](https://docs.gitlab.com/ee/ci/yaml/#cache): Dateien zwischen Pipelines austauschen 
- [Extend](https://docs.gitlab.com/ee/ci/yaml/#extends): Komplexe Jobs bauen
- [Include](https://docs.gitlab.com/ee/ci/yaml/#include): Komplexe Konfigurationen bauen
- [Pipeline Triggers](https://docs.gitlab.com/ee/ci/triggers/): Pipelines über mehrere Projekte

## GitLab Pipeline

![screenshot of a GitLab pipeline](pipeline.png){style="height: 75vh;"}

## GitLab Pipeline

```
build:
- review-docs-deploy-manual

prepare:
- compile-assets
- retrieve-tests-metadata
- setup-test-env

test:
- cache gems
- code_quality
- db:check-schema-pg
- db:migrate:reset-mysql
- db:migrate:reset-pg
- db:rollback-mysql
- db:rollback-pg
- dependency_scanning
- docs lint
- gitlab:assets:compile
- gitlab:setup-mysql
- gitlab:setup-pg
- gitlab_git_test
- jest
- karma
- migration:path-mysql
- migration:path-pg
- no_ee_check
- package-and-qa
- qa-frontend-node:8
- qa-frontend-node:10
- qa-frontend-node:latest
- qa:internal
- qa:selectors
- rspec-fast-spec-helper
- rspec-mysql
- rspec-mysql-quarantine
- rspec-pg
- rspec-pg-/50
- rspec-pg-quarantine
- sast
- static-analysis

post-test:
- coverage
- jsdoc
- lint:javascript:report
- update-tests-metadata

pages:
- pages

post-cleanup:
- review-docs-cleanup

deploy:
- pages:deploy
```

## GitLab Pipeline

```yaml
image: "dev.gitlab.org:5005/gitlab/gitlab-build-images:ruby-2.5.3-golang-1.11-git-2.21-chrome-71.0-node-10.x-yarn-1.12-postgresql-9.6-graphicsmagick-1.3.29"

# ...

include:
  - local: .gitlab/ci/global.gitlab-ci.yml
  - local: .gitlab/ci/cng.gitlab-ci.yml
  - local: .gitlab/ci/docs.gitlab-ci.yml
  - local: .gitlab/ci/frontend.gitlab-ci.yml
  - local: .gitlab/ci/pages.gitlab-ci.yml
  - local: .gitlab/ci/qa.gitlab-ci.yml
  - local: .gitlab/ci/reports.gitlab-ci.yml
  - local: .gitlab/ci/rails.gitlab-ci.yml
  - local: .gitlab/ci/review.gitlab-ci.yml
  - local: .gitlab/ci/setup.gitlab-ci.yml
  - local: .gitlab/ci/test-metadata.gitlab-ci.yml
```

<https://gitlab.com/gitlab-org/gitlab-ce/blob/master/.gitlab-ci.yml>

# Fragen?

später gerne an:\
`winnie(🐒)gitlab📍com`
