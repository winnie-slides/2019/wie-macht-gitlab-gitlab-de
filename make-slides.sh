#!/usr/bin/env sh

pandoc \
  --standalone \
  --mathjax \
  --to=revealjs \
  --variable=theme:white \
  --variable=backgroundTransition:none \
  --css=style.css \
  --output=public/index.html \
  --slide-level=2 \
  slides.md
